# letmeoutofyour.net server build

## Original Posts

- Intro: https://malicious.link/post/2012/2012-08-10-let-me-out-of-your-net-intro/
- Server build: https://malicious.link/post/2012/2012-08-11-let-me-out-of-your-net-server-build/

## Clients

- Author: @mubix - AutoIT3 Client: https://github.com/mubix/post-exploitation/blob/master/win32bins/network/letmeoutofyournet/w00tw00t_incremental.au3
- Author: @sensepost - Go Client "go-out": https://github.com/sensepost/go-out
- Author: @jakxx - PowerShell Client - "lemmeout.ps1": https://github.com/jakxx/Scripts/blob/master/lemmeout.ps1
- Author: @jakxx - Python Client "lemmeout.py": https://github.com/jakxx/Scripts/blob/master/lemmeout.py
- Author: @lanmaster53 - Python Client "getout.py": https://github.com/lanmaster53/ptscripts/blob/master/getout.py
- Author: @santosomar - Bash script: https://github.com/The-Art-of-Hacking/art-of-hacking/blob/master/useful_commands_and_scripts/letmeout.sh

Ansible script to create a clone of https://letmeoutofyour.net

Requirements on system:
 - python
 - aptitude

## Current lacking features
  - UDP listener
  - Wild card domains for LetsEncrypt

## Example Run

```
mubix@localhost$ ansible-playbook -i targets.txt lmofy.playbook

PLAY [LetMeOutOfYour.net Server Builder] ************************************************

TASK [Gathering Facts] ******************************************************************
ok: [50.50.50.50]

TASK [apt : Update repositories cache] **************************************************
changed: [50.50.50.50]

TASK [apt : Upgrade all packages to the latest version] *********************************
ok: [50.50.50.50]

TASK [apt : Install required packages] **************************************************
ok: [50.50.50.50] => (item=[u'nginx', u'iptables', u'letsencrypt', u'sslh'])

TASK [nginx : Generate dhparams] ********************************************************
ok: [50.50.50.50]

TASK [nginx : Write checkstring to index.html] ******************************************
ok: [50.50.50.50]

TASK [nginx : Write Nginx Site File] ****************************************************
ok: [50.50.50.50]

TASK [nginx : Write Nginx SSL Site File] ************************************************
changed: [50.50.50.50]

TASK [nginx : Restart Nginx Daemon, in all cases] ***************************************
changed: [50.50.50.50]

TASK [nginx : Add letsencrypt cronjob for cert renewal] *********************************
ok: [50.50.50.50]

TASK [letsencrypt : Create letsencrypt certificate] *************************************
ok: [50.50.50.50]

TASK [letsencrypt : Enable Nginx SSL Site] **********************************************
ok: [50.50.50.50]

TASK [letsencrypt : Reload Nginx Daemon to enable SSL site] *****************************
changed: [50.50.50.50]

TASK [sslh : Install SSLH Config] *******************************************************
changed: [50.50.50.50]

TASK [sslh : Set SSH Daemon to Listen on localhost IPv6] ********************************
changed: [50.50.50.50]

TASK [sslh : Set SSH Daemon to Listen on localhost IPv4] ********************************
changed: [50.50.50.50]

TASK [sslh : Configure to SSLH Service] *************************************************
ok: [50.50.50.50]

TASK [iptables : get my public IP] ******************************************************
ok: [50.50.50.50]

TASK [iptables : Add iptables rule] *****************************************************
changed: [50.50.50.50]

TASK [Restart SSH Daemon, in all cases] *************************************************
changed: [50.50.50.50]

TASK [Restart SSLH Daemon, in all cases] ************************************************
changed: [50.50.50.50]

PLAY RECAP ******************************************************************************
50.50.50.50              : ok=21   changed=10   unreachable=0    failed=0

```